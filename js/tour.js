$(document).ready(function () {
    //-------------------------------- Dropdowns form -------------------------------
    function formDropdowns(id, list, item, select, block, dropdown) {

        $(id).click(function () {
            $(list).slideToggle(150);
            $(this).find('img').toggleClass('active-dropdown');
        });
        $(item).click(function () {
            $(select).val($(this).text());
        });

        $(document).on("click", function (event) {
            let $trigger = $(dropdown);
            if ($trigger !== event.target && !$trigger.has(event.target).length) {
                $(block).slideUp(150);
            }
        });
    }

    formDropdowns('.count-dropdown-list', ".count-list", '.count-item', '.count-input', '.count-list', ".count-dropdown");



    // ---------------------Scroll--------------------------

    $("a[href^='#']").click(function () {
        $("html, body").animate({
            scrollTop: $($(this).attr("href")).offset().top + "px"
        }, {
            duration: 1000,
            easing: "swing"
        });
        return false;
    });

    // -----------------Fix block reservation --------------------

    if ($(window).width() > 992) {
        // $(function () {
        //     $(window).scroll(function () {
    
        //         let top = $(document).scrollTop();
        //         let reg = $('.reg').offset().top;
        //         let tour = $('.tour').offset().top;
    
        //         if (top > tour + 80) $('.reservation-wrap').addClass('reservation_fixed');
    
        //         else $('.reservation-wrap').removeClass('reservation_fixed');
    
        //         if (top > reg - 800) $('.reservation-wrap').removeClass('reservation_fixed');
    
        //     });
        // });
    } else {

        $(function () {
            $(window).scroll(function () {
    
                let top = $(document).scrollTop();
                let reg = $('.reg').offset().top;
                let tour = $('.tour').offset().top;
                
                if (top > tour + 80) $('.reservation-wrap').addClass('reservation_fixed-mob');
    
                else $('.reservation-wrap').removeClass('reservation_fixed-mob');
    
                if (top > reg - 800) $('.reservation-wrap').removeClass('reservation_fixed-mob');
            });
        });
    }
    
    


});