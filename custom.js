'use strict';

$(document).ready(function () {

    $('.team__list').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		infinite: true,
		draggable: false,
		arrows: true,
		prevArrow: "<button type='button' class='slick-prev team-slider-prev'>←</button>",
		nextArrow: "<button type='button' class='slick-next team-slider-next'>→</button>",
		responsive: [

			{
				breakpoint: 992,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
					}
				},
			{
			breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				}
		    },
			{
			breakpoint: 576,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
		    },
		]

		 
	});
});
